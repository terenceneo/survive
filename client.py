#!/usr/bin/env python
import random
from pathlib import Path

from game.game import Game
from game.world import World
from game.utils import save_list
from game.agent_ai import PLAYER_AIS

class Client:
    def __init__(self, game: Game, save_dir = None):
        self.game = game
        self.character_default_settings = None
        self.world_code = None
        self.game_history = None
        self.endgame_stats = None
        self.save_dir = save_dir

    def run(self, auto_save=True):
        print(self.game.world)

        self.character_default_settings = self.game.world.character_settings.splitlines()
        self.world_code = self.game.world.code.splitlines()

        # Start Game
        self.game.start()

        self.game_history = self.game.logger
        self.endgame_stats = self.game.endgame_logger

        if auto_save:
            self.save(self.save_dir)


    def save(self, directory=''):
        if directory:
            d = Path(directory)
            d.mkdir(exist_ok=True)
        else:
            d = Path()

        save_list(self.character_default_settings, d / 'Character_Default_Settings.txt')
        save_list(self.world_code, d / 'map_grid.txt')

        save_list(self.game_history, d / 'game_history.txt')
        save_list(self.endgame_stats, d / 'results.txt')


if __name__ == '__main__':
    from game.objects import Sword, Strawberry, Water, MonsterM, Player
    from game.terrain import Rock
    from world_builder import create_test_game, create_game_with_one_strong_monster_many_weak_monsters_and_two_imba_sword,\
        create_battle_royale

    #game = create_test_game()
    #game = create_game_with_one_strong_monster_many_weak_monsters_and_two_imba_sword()
    game = create_battle_royale()
    client = Client(game=game)
    client.run(auto_save=False)
    client.save('saves')
