# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 16:29:02 2019

@author: czhengxi
"""

from PathFinding.pathfind import PathFind
from PathFinding.search import astar_search, breadth_first_graph_search, uniform_cost_search
from world_builder import create_maze_prim
import random
import time

def generate_grid(length, width, num_of_obstacles, seed=None):
    random.seed(seed)
    grid = [[True for x in range(width)] for y in range(length)]
    for obs in range(num_of_obstacles):
        rand_num = random.randint(0, length*width-1)
        x = rand_num % width
        y = int(rand_num / width)
        grid[y][x] = False
    return grid

def print_grid(grid, start_pt=None, end_pt=None):
    for (y, row) in enumerate(grid):    
        for (x, cell) in enumerate(row):
            if (y, x) == start_pt:
                symbol = 's'
            elif (y, x) == end_pt:
                symbol = 'e'
            elif cell:
                symbol = '.'
            else:
                symbol = 'x'
            print("%s "%symbol, end="")
        print()
        
def print_route(sol, grid):
    grid = grid.copy()
    path = sol.path()
    for i, n in enumerate(path):
        (y, x) = n.state        
        grid[y][x] = 's' if i==0 else 'e' if i==len(path)-1 else 'o'
    for row in grid:    
        for cell in row:
            if cell == True:
                symbol = '.'
            elif cell == False:
                symbol = 'x'
            else:
                symbol = cell
            print("%s "%symbol, end="")
        print()

length = 13
width = 21
for length in range (51,100000000,2):
  for width in range (51, 10000000, 2):
    start_pt = (7,7)
    end_pt = (21,21)
    grid = create_maze_prim(length, width, unblocked_locs=[start_pt, end_pt], display_maze=False, borders=False) 
    print("length:", length)         
    print("width:", width)
    print_grid(grid, start_pt, end_pt)
    print("start point:", start_pt)
    print("end point:", end_pt)
    print()
    prob = PathFind(grid,start_pt, end_pt)
    t0 = time.time()
    bfs_sol = breadth_first_graph_search(prob)
    t1 = time.time()
    a_star_sol = astar_search(prob)
    t2 = time.time()
    ucs_sol = uniform_cost_search(prob)
    t3 = time.time()

    if a_star_sol:        
       print("BFS Solution <%d sec elapsed>"%(t1-t0))
       #print(bfs_sol.path())   
       print()
       print("A* Solution <%d sec elapsed>"%(t2-t1))
       #print(a_star_sol.path())
       print()
       print("UCS Solution <%d sec elapsed>"%(t3-t2))
       # print_route(ucs_sol, grid)
    else:
       raise Exception('NO PATH!')

    
#Evaluate time complexities
# grid = generate_grid(200, 200, 100)   
# bfs_time = 0
# a_star_time = 0
# num_of_iters = 10
# for i in range(num_of_iters):
#     start_pt = pick_random_point(grid)
#     end_pt = pick_random_point(grid)
#     print("start point:", start_pt)
#     print("end point:", end_pt)
#     print()
#     prob = PathFind(grid,start_pt, end_pt)
#     t0 = time.time()
#     bfs_sol = breadth_first_graph_search(prob)
#     t1 = time.time()
#     a_star_sol = astar_search(prob)
#     t2 = time.time()
    
#     if a_star_sol:
#         bfs_time += (t1-t0)
#         a_star_time += (t2-t1)        
#     else:
#         print("No path found!")    

# print("BFS Solution <%f sec elapsed>"%(bfs_time/num_of_iters))        
# print("A* Solution <%f sec elapsed>"%(a_star_time/num_of_iters))
