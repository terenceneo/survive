#!/usr/bin/env python
import math
import random

from game.game import Game
from game.world import World
from game.utils import save_list
from game.agent_ai import PLAYER_AIS
from game.objects import *
from game.terrain import Rock
from PathFinding.pathfind import PathFind
from PathFinding.search import astar_search
from game.agent_ai import PLAYER_AIS

class world_builder:
    def __init__(self, length=10, width=10, add_border=True):
        self.world = World(shape=(length,width), add_border=add_border)
        self.has_border = add_border
        self.length = length
        self.width = width

    '''
    Add rocks
    Input:
        num_of_rocks - number of rocks
        locs - (Optional) List of locations for the rocks, will be random if None
    '''
    def add_rocks(self, num_of_rocks, locs=None):    
        if locs:
            if len(locs) != num_of_rocks:
                raise ValueError(f'Size of locs must equal number of rocks({num_of_rocks})')
            selected_locs = locs
        else:      
            available_locs = list(filter(lambda i: not self.world.get_cell(int(i/self.width),int(i%self.width)).blocker, range(self.width*self.length)))
            try:
                selected_locs = [(int(x/self.width),int(x%self.width)) for x in random.sample(available_locs, num_of_rocks)]
            except:
                raise ValueError(f'No more space to add {num_of_rocks} rocks')
        for i in selected_locs:
            self.world.set_terrain(Rock, i)

    '''
    Add items
    Input:
        items - either the number of random items or a list of Inventory objects
        locs - (Optional) List of locations for the items, will be random if None
    '''
    def add_items(self, items, locs=None):
        if isinstance(items, int):
            num_of_items = items
            selected_items = [random.choice([Sword(), Strawberry(), Water()]) for i in range(num_of_items)]
        else:
            num_of_items = len(items)
            selected_items = items
        if locs:
            if len(locs) != num_of_items:
                raise ValueError(f'Size of locs must equal number of items({num_of_items})')
            selected_locs = locs
        else:
            available_locs = list(filter(lambda i: not self.world.get_cell(int(i/self.width),int(i%self.width)).blocker, range(self.width*self.length)))       
            try:
                selected_locs = [(int(x/self.width),int(x%self.width)) for x in random.choices(available_locs, k=num_of_items)]
            except:
                raise ValueError(f'No more space to add {num_of_items} random items')
        for (x,y)in zip(selected_items, selected_locs):
            self.world.add_object(x, y)

    def get_world(self):
        return self.world
    '''
    Add players
    Input:
        players - either the number of players or a list of Player objects
        locs - (Optional) List of locations for the players, will be random if None
    '''
    def add_players(self, players, locs=None):
        if isinstance(players, int):
            num_of_players = players
            selected_players = [Player(player_id=i+1, agent_ai=PLAYER_AIS[0]) for i in range(num_of_players)]
        else:
            num_of_players = len(players)
            selected_players = players
        if locs:
            if len(locs) != num_of_players:
                raise ValueError(f'Size of locs must equal number of players({num_of_players})')
            selected_locs = locs
        else:
            available_locs = list(filter(lambda i: not self.world.get_cell(int(i/self.width),int(i%self.width)).blocker, range(self.width*self.length)))             
            try:
                selected_locs = [(int(x/self.width),int(x%self.width)) for x in random.sample(available_locs, num_of_players)]
            except:
                raise ValueError(f'No more space to add {num_of_players} players')
        for (x,y) in zip(selected_players, selected_locs):             
            self.world.add_agent(x, y)

    '''
    Add monsters
    Input:
        monsters - either the number of monsters or a list of MonsterM objects
        locs - (Optional) List of locations for the monsters, will be random if None
    '''
    def add_monsters(self, monsters, locs=None):
        if isinstance(monsters, int):
            num_of_monsters = monsters
            selected_monsters = [MonsterM(monster_id=i+1) for i in range(num_of_monsters)]
        else:
            num_of_monsters = len(monsters)
            selected_monsters = monsters
        if locs:
            if len(locs) != num_of_monsters:
                raise ValueError(f'Size of locs must equal number of monsters({num_of_monsters})')
            selected_locs = locs
        else:
            available_locs = list(filter(lambda i: not self.world.get_cell(int(i/self.width),int(i%self.width)).blocker, range(self.width*self.length)))             
            try:
                selected_locs = [(int(x/self.width),int(x%self.width)) for x in random.sample(available_locs, num_of_monsters)]
            except:
                raise ValueError(f'No more space to add {num_of_monsters} monsters')
        for (x,y) in zip(selected_monsters, selected_locs):             
            self.world.add_agent(x, y)

    def get_players(self):
        return [agent for agent in self.world.agents if isinstance(agent, Player)]

    def get_monsters(self):
        return [agent for agent in self.world.agents if isinstance(agent, MonsterM)]

GRID_LENGTH = 14
GRID_WIDTH = 22

def create_maze_prim(length, width, unblocked_locs=[], display_maze=False, borders=True):
    if length%2==0 or width%2==0:
        raise ValueError('Length and width must both be odd numbers')
    if borders:
        length -= 2
        width -= 2
        if length<=0 or width<=0:
            raise ValueError('Length and width must both be greater than 2 if using borders')
    #create maze with thin walls
    thin_maze_length = int((length+1)/2)
    thin_maze_width = int((width+1)/2)
    #initialise walls
    has_walls = set()
    visited = set()
    wall_list = []
    for y in range(thin_maze_length):
        for x in range(thin_maze_width):
            if y < thin_maze_length-1:
                has_walls.add((y,x,"DOWN"))        
            if x < thin_maze_width-1:
                has_walls.add((y,x,"RIGHT"))                                            
    tmp = random.choice(range(thin_maze_width*thin_maze_length))
    (y,x) = (int(tmp/thin_maze_width),int(tmp%thin_maze_width))
    visited.add((y,x))
    if y > 0:
        wall_list.append((y-1,x,"DOWN"))
    if x > 0:
        wall_list.append((y,x-1,"RIGHT"))
    if y < thin_maze_length-1:
        wall_list.append((y,x,"DOWN"))
    if x < thin_maze_width-1:
        wall_list.append((y,x,"RIGHT"))
    while len(wall_list) > 0:
        (y,x,dir) = wall_list.pop(random.randrange(len(wall_list)))
        cell_to_be_visited = None
        if dir=="DOWN" and not ((y,x) in visited and (y+1,x) in visited):
            has_walls.remove((y,x,"DOWN"))
            if (y,x) not in visited:
                cell_to_be_visited = (y,x)
            else:
                cell_to_be_visited = (y+1,x)            
        elif dir=="RIGHT" and not ((y,x) in visited and (y,x+1) in visited):
            has_walls.remove((y,x,"RIGHT"))
            if (y,x) not in visited:
                cell_to_be_visited = (y,x)
            else:
                cell_to_be_visited = (y,x+1)
        if cell_to_be_visited:
            visited.add(cell_to_be_visited) 
            (y,x) = cell_to_be_visited
            if y > 0 and (y-1,x,"DOWN") in has_walls:
                wall_list.append((y-1,x,"DOWN"))
            if x > 0 and (y,x-1,"RIGHT") in has_walls:
                wall_list.append((y,x-1,"RIGHT"))
            if y < thin_maze_length-1 and (y,x,"DOWN") in has_walls:
                wall_list.append((y,x,"DOWN"))
            if x < thin_maze_width-1 and (y,x,"RIGHT") in has_walls:
                wall_list.append((y,x,"RIGHT"))
    #expand the walls of the maze
    grid = [[True for x in range(width)] for y in range(length)]
    for y in range(thin_maze_length):
        for x in range(thin_maze_width):
            if (y,x,"DOWN") in has_walls:
                grid[2*y+1][2*x] = False
            if (y,x,"RIGHT") in has_walls:
                grid[2*y][2*x+1] = False
    for y in range(length):
        for x in range(width):
            if y%2==1 and x%2==1:
                grid[y][x] = False
    if borders:
        grid.insert(0, [False for _ in range(width)])
        grid.append([False for _ in range(width)])
        grid = [[False]+row+[False] for row in grid]
    for (y,x) in unblocked_locs:
        grid[y][x] = True
        surrounding_rock_pos = []
        num_of_neighbours = 0
        # check if surrounding are all rocks
        if (y>=1 and not borders) or (y>=2 and borders):
            num_of_neighbours+=1
            if not grid[y-1][x]:
                surrounding_rock_pos.append((y-1,x))
        if (y<=length-2 and not borders) or (y<=length-1 and borders):
            num_of_neighbours+=1
            if not grid[y+1][x]:
                surrounding_rock_pos.append((y+1,x))
        if (x>=1 and not borders) or (x>=2 and borders):
            num_of_neighbours+=1
            if not grid[y][x-1]:
                surrounding_rock_pos.append((y,x-1))
        if (x<=width-2 and not borders) or (x<=width-1 and borders):
            num_of_neighbours+=1
            if not grid[y][x+1]:
                surrounding_rock_pos.append((y,x+1))
        if num_of_neighbours>0 and num_of_neighbours==len(surrounding_rock_pos):
            (y,x) = surrounding_rock_pos[0]
            grid[y][x] = True
            # print('Added 1 more rock!')
    if display_maze:
        for row in grid:
            for cell in row:
                print('.' if cell else 'r', end=''),
                print(end=' '),
            print()            
    return grid     

def shortest_path(grid, src, dest, get_len=False):
    prob = PathFind(grid, src, dest)
    search_tree = astar_search(prob)
    path = search_tree.solution() if search_tree else None
    if get_len:
        return len(path)
    return path

def create_game_with_one_strong_monster_and_two_imba_sword():
    wb = world_builder(GRID_LENGTH, GRID_WIDTH, True)
    midpt = (math.ceil((GRID_LENGTH-2)/2),math.ceil((GRID_WIDTH-2)/2))
    corners = [(1,1), (GRID_LENGTH-2,1), (GRID_LENGTH-2,GRID_WIDTH-2), (1,GRID_WIDTH-2)]
    wb.add_players(4, corners) 
    wb.add_monsters([
        BossMonster(monster_id=1),
        ], 
        [
        midpt, 
        ])
    wb.add_items([GreatSword(), GreatSword()], [(6,2), (6,18)])
    world = wb.get_world()
    game = Game(world, players=wb.get_players(), max_rounds=100)
    return game    


def create_game_with_one_strong_monster_many_weak_monsters_and_two_imba_sword():
    wb = world_builder(GRID_LENGTH, GRID_WIDTH, True)
    midpt = (math.ceil((GRID_LENGTH-2)/2),math.ceil((GRID_WIDTH-2)/2))
    corners = [(1,1), (GRID_LENGTH-2,1), (GRID_LENGTH-2,GRID_WIDTH-2), (1,GRID_WIDTH-2)]
    wb.add_players(4, corners) 
    wb.add_rocks(10)
    wb.add_monsters([
        BossMonster(monster_id=1),
        MonsterM(monster_id=1),
        MonsterM(monster_id=2),
        MonsterM(monster_id=3),
        MonsterM(monster_id=4),
        MonsterM(monster_id=5),
        MonsterM(monster_id=6),
        MonsterM(monster_id=7),
        MonsterM(monster_id=8),
        ], 
        [
        midpt, 
        (midpt[0],midpt[1]+1),
        (midpt[0]+1,midpt[1]),
        (midpt[0]+1,midpt[1]+1),
        (midpt[0]-1,midpt[1]),
        (midpt[0],midpt[1]-1),
        (midpt[0]-1,midpt[1]-1),
        (midpt[0]+1,midpt[1]-1),
        (midpt[0]-1,midpt[1]+1),
        ])
    wb.add_items([GreatSword(), GreatSword()], [(6,2), (6,18)])
    world = wb.get_world()
    save_list(world.character_settings.splitlines(), 'Character_Default_Settings.txt')
    save_list(world.code.splitlines(), 'map_grid.txt')
    game = Game(world, players=wb.get_players(), max_rounds=100)
    return game    


def create_game_with_many_weak_monsters():
    wb = world_builder(GRID_LENGTH, GRID_WIDTH, True)
    midpt = (math.ceil((GRID_LENGTH-2)/2),math.ceil((GRID_WIDTH-2)/2))
    corners = [(1,1), (1,GRID_WIDTH-2), (GRID_LENGTH-2,GRID_WIDTH-2), (GRID_LENGTH-2,1)]
    wb.add_players(4, corners)   
    wb.add_items(30)   
    wb.add_monsters([MonsterM(monster_id=i+1) for i in range(25)])    
    world = wb.get_world()
    game = Game(world, players=wb.get_players(), max_rounds=100)
    return game

def create_maze_game():
    maze_length = 13
    maze_width = 21
    end_pt = (math.ceil((maze_length-2)/2),math.ceil((maze_width-2)/2))
    start_pt = (0,0)
    # create maze
    maze = create_maze_prim(maze_length, maze_width, [start_pt, end_pt], display_maze=True, borders=False)
    print(start_pt, end_pt)
    wb = world_builder(GRID_LENGTH, GRID_WIDTH, True)
    world = wb.get_world()
    maze_shortest_path_len = shortest_path(maze, start_pt, end_pt, get_len=True)
    for y in range(GRID_LENGTH):
        for x in range(GRID_WIDTH):
            # print(y,x)
            if y>maze_length or y==0 or x>maze_width or x==0:
                world.set_terrain(Rock, (y,x))
            else:
                maze_y = y-1
                maze_x = x-1
                if not maze[maze_y][maze_x]:
                    world.set_terrain(Rock, (y,x))
    wb.add_players(1, [offset_cell(start_pt, 1)])
    wb.add_items([GreatSword()],[offset_cell(end_pt, 1)])
    game = Game(world, players=wb.get_players(), max_rounds=maze_shortest_path_len, win_condition=None)
    return game


def offset_cell(cell, offset):
    if isinstance(offset, int):
        return tuple([a+offset for a in cell])
    if len(cell)!= len(offset):
        raise ValueError('cell and offset must have same dimensions')
    tmp = tuple([a+b for (a,b) in zip(cell,offset)])
    print(tmp)
    return tmp


def create_tutorial_game(player_one_ai=None, decision_timeout=None):
    maze_length = 11
    maze_width = 15
    wb = world_builder(GRID_LENGTH, GRID_WIDTH, True)
    monster_cell = (maze_length-1,maze_width-1)
    player_cell = (0,0)
    food_cell = (2,2)
    treasure_cell = (5,7)
    #create some space aroound monster
    monster_cells = [(monster_cell[0]-1,monster_cell[1]-1),(monster_cell[0]-1,monster_cell[1]),
    (monster_cell[0],monster_cell[1]-1),(monster_cell[0],monster_cell[1])]
    # create maze with space for monster, start_pt, end_pt, strawberry
    maze = create_maze_prim(maze_length, maze_width, [player_cell,treasure_cell,food_cell]+monster_cells, display_maze=True, borders=False)
    world = wb.get_world()
    maze_shortest_path_len = shortest_path(maze, player_cell, treasure_cell, get_len=True)
    print(f'Length of shortest path to great sword: {maze_shortest_path_len}')
    for y in range(GRID_LENGTH):
        for x in range(GRID_WIDTH):
            # print(y,x)
            if y>maze_length or y==0 or x>maze_width+2 or x<=2:
                world.set_terrain(Rock, (y,x))
            else:
                maze_y = y-1
                maze_x = x-3
                if not maze[maze_y][maze_x]:
                    world.set_terrain(Rock, (y,x))
    p1 = Player(player_id=1, agent_ai=player_one_ai or PLAYER_AIS[0], timeout=decision_timeout)
    wb.add_players([p1], [offset_cell(player_cell, (1,3))])
    wb.add_items([Strawberry(), GreatSword()],[offset_cell(food_cell, (1,3)),offset_cell(treasure_cell, (1,3))])
    wb.add_monsters([MonsterM()], [offset_cell(monster_cell, (1,3))])
    game = Game(world, players=wb.get_players(), max_rounds=50, win_condition=None)
    return game


def create_test_game(player_one_ai=None, player_two_ai=None, player_three_ai=None, player_four_ai=None):
    world = World(shape=(10, 10), add_border=False)

    ###########
    # Terrain #
    ###########
    world.add_border()
    world.set_terrain(Rock, (2, 2))
    world.set_terrain(Rock, (2, 3))
    world.set_terrain(Rock, (3, 2))

    ##########
    # Agents #
    ##########

    # Players
    p1 = Player(player_id=1, agent_ai=player_one_ai or PLAYER_AIS[0])
    p2 = Player(player_id=2, agent_ai=player_two_ai or PLAYER_AIS[1])
    p3 = Player(player_id=3, agent_ai=player_three_ai or PLAYER_AIS[2])
    p4 = Player(player_id=4, agent_ai=player_four_ai or PLAYER_AIS[3])

    world.add_agent(p1, (4, 4))
    world.add_agent(p2, (6, 6))
    world.add_agent(p3, (4, 6))
    world.add_agent(p4, (6, 4))

    players = [p1, p2, p3, p4]

    # Monsters
    m1 = MonsterM(monster_id=1)
    m2 = MonsterM(monster_id=2)
    world.add_agent(m1, (1, 1))
    world.add_agent(m2, (8, 8))

    #########
    # Items #
    #########

    # swords
    sw1 = Sword()
    sw2 = Sword()
    world.add_object(sw1, (5, 6))
    world.add_object(sw2, (6, 5))

    # strawberries
    sb1 = Strawberry()
    sb2 = Strawberry()
    world.add_object(sb1, (7, 7))
    world.add_object(sb2, (3, 3))

    # water
    wt1 = Water()
    wt2 = Water()
    world.add_object(wt1, (5, 6))
    world.add_object(wt2, (5, 8))

    return Game(world, players=players, max_rounds=100)


def create_battle_royale(
        player_one_ai=None,
        player_two_ai=None,
        player_three_ai=None,
        player_four_ai=None,
        player_one_name=None,
        player_two_name=None,
        player_three_name=None,
        player_four_name=None,
        decision_timeout=1,
        max_rounds=75,
    ):
    wb = world_builder(GRID_LENGTH, GRID_WIDTH, True)
    p1 = Player(name=player_one_name, player_id=1, agent_ai=player_one_ai or PLAYER_AIS[0], timeout=decision_timeout)
    p2 = Player(name=player_two_name, player_id=2, agent_ai=player_two_ai or PLAYER_AIS[1], timeout=decision_timeout)
    p3 = Player(name=player_three_name, player_id=3, agent_ai=player_three_ai or PLAYER_AIS[2], timeout=decision_timeout)
    p4 = Player(name=player_four_name, player_id=4, agent_ai=player_four_ai or PLAYER_AIS[3], timeout=decision_timeout)
    m1 = MonsterM(monster_id=1)
    m2 = MonsterM(monster_id=2)
    m3 = MonsterM(monster_id=3)
    m4 = MonsterM(monster_id=4)
    # thicken right border
    wb.add_rocks(12, [(i,20) for i in range(1,13)])
    # thicken lower border
    wb.add_rocks(19, [(12,i) for i in range(1,20)])

    # add players
    wb.add_players([p1,p2,p3,p4], [(3,4),(3,16),(9,4),(9,16)])
    # add monsters
    wb.add_monsters([m1,m2,m3,m4], [(5,9),(5,11),(7,9),(7,11)])
    # add weapons
    wb.add_items([Sword(),Sword(),Sword(),Sword(), GreatSword()], [(3,10),(6,13),(9,10),(6,7),(6,10)])
    # add strawberries
    strawberry_positions = [
        # (2,4),(2,16),(8,4),(8,16),     # beside players
        (1,1),(1,2),(2,2),(2,1),     # top left
        (6,1),(6,2),#(7,2),(7,1),    # middle left
        (11,1),(11,2),(10,2),(10,1), # lower left
        (1,18),(1,19),(2,19),(2,18), # top right 
        (6,18),(6,19),#(7,19),(7,18),# middle right
        (11,18),(11,19),(10,19),(10,18), # lower right
        (1,9),(1,10),(1,11),(2,9),(2,10),(2,11),       # top mid
        (11,9),(11,10),(11,11),(10,9),(10,10),(10,11), # lower mid
        (5,10),(6,11),(7,10),(6,9)   # middle
    ]
    wb.add_items([Strawberry() for _ in range(len(strawberry_positions))], strawberry_positions)
    # add water
    wb.add_items([Water(),Water(),Water(),Water()], [(3,5),(3,15),(9,5),(9,15)])
    game = Game(wb.get_world(), players=wb.get_players(), max_rounds=max_rounds)
    return game


def create_battle_royale_two_players(
        player_one_ai=None,
        player_two_ai=None,
        player_one_name=None,
        player_two_name=None,
        decision_timeout=1,
        max_rounds=75,
    ):
    wb = world_builder(GRID_LENGTH, GRID_WIDTH, True)
    p1 = Player(name=player_one_name, player_id=1, agent_ai=player_one_ai or PLAYER_AIS[0], timeout=decision_timeout)
    p2 = Player(name=player_two_name, player_id=4, agent_ai=player_two_ai or PLAYER_AIS[3], timeout=decision_timeout)
    m1 = MonsterM(monster_id=1)
    m2 = MonsterM(monster_id=2)
    m3 = MonsterM(monster_id=3)
    m4 = MonsterM(monster_id=4)
    # thicken right border
    wb.add_rocks(12, [(i,20) for i in range(1,13)])
    # thicken lower border
    wb.add_rocks(19, [(12,i) for i in range(1,20)])

    # add players
    wb.add_players([p1,p2], [(3,4), (9,16)])
    # add monsters
    wb.add_monsters([m1,m2,m3,m4], [(5,9), (5,11), (7,9), (7,11)])
    # add weapons
    wb.add_items(
        [Sword(), Sword(), Sword(), Sword(), GreatSword()],
        [(3,10), (6,13), (9,10), (6,7), (6,10)]
    )
    # add strawberries
    strawberry_positions = [
        # (2,4),(2,16),(8,4),(8,16), # beside players
        (1,1),(1,2),(2,2),(2,1),     # top left
        (6,1),(6,2),#(7,2),(7,1),    # middle left
        (11,1),(11,2),(10,2),(10,1), # lower left
        (1,18),(1,19),(2,19),(2,18), # top right 
        (6,18),(6,19),#(7,19),(7,18),# middle right
        (11,18),(11,19),(10,19),(10,18), # lower right
        (1,9),(1,10),(1,11),(2,9),(2,10),(2,11),       # top mid
        (11,9),(11,10),(11,11),(10,9),(10,10),(10,11), # lower mid
        (5,10),(6,11),(7,10),(6,9)   # middle
    ]
    wb.add_items([Strawberry() for _ in range(len(strawberry_positions))], strawberry_positions)
    # add water
    wb.add_items([Water(),Water(),Water(),Water()], [(3,5),(3,15),(9,5),(9,15)])
    game = Game(wb.get_world(), players=wb.get_players(), max_rounds=max_rounds)
    return game


if __name__ == '__main__':
    from client import Client
    # Start Game
    # game = create_game_with_many_weak_monsters()
    # game = create_game_with_one_strong_monster_many_weak_monsters_and_two_imba_sword()
    # game = create_maze_game()
    # game = create_game_with_one_strong_monster_and_two_imba_sword()
    # game = create_tutorial_game()
    # game = create_battle_royale_two_players()
    game = create_battle_royale()
    client = Client(game)
    client.run()
    print(f'Number of rounds played: {game.round_num}')
