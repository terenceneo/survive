from __future__ import annotations
from enum import Enum
from dataclasses import dataclass, field
from typing import List, Dict, TYPE_CHECKING

from prettyprinter import register_pretty, pretty_call

from game.objects_base import InventoryItem, PointItem, UsableItem, Agent

if TYPE_CHECKING:
  from game.world import World


class Direction(Enum):
  UP = (-1, 0)
  DOWN = (1, 0)
  LEFT = (0, -1)
  RIGHT = (0, 1)


dis2dir = {enum.value: enum for enum in Direction}


class CommandException(Exception):
  ...


@dataclass
class Command:
  agent: Agent = field(default=None, init=False)
  world: World = field(default=None, init=False, repr=False)
  logger: List[str] = field(default_factory=list, init=False)

  def execute(self):
    assert self.agent and self.world
    try:
      self._execute()
    except CommandException as e:
      print(e)
      raise CommandException

  def _execute(self):
    pass


@dataclass
class IdleCommand(Command):
  def _execute(self):
    self.logger.append(f'{self.agent.code}, IDLE')


@dataclass
class AttackCommand(Command):
  defender: Dict = None

  def _execute(self):
    attacker = self.agent
    defender: Agent = self.world.id2obj[self.defender['uid']]
    assert self.world.distance(self.agent, defender) == 1, (self.agent.pos, defender.pos)

    atk = self.agent.atk

    direction = dis2dir[self.world.displacement(attacker, defender)]

    new_health = max(0, defender.health - atk)
    delta = defender.health - new_health
    defender.health = new_health
    self.logger.append(f'{self.agent.code}, ATTACK, {defender.code}, {direction.name}')
    self.logger.append(f'{defender.code}, HP, MINUS, {delta}, {defender.health}')

    print(f"{self.agent.name} dealt {atk} dmg to {defender.name}!")
    print(f"{defender.name} current health: {defender.health}.")

    if defender.health <= 0:
      print(f"{defender.name} is dead.")
      logs = self.world.kill_agent(defender)
      self.logger.extend(logs)
      if attacker.__class__.__name__ == 'Player':  # circular import hack
        self.world.add_points(attacker, defender.points, logger=self.logger)


@dataclass
class MoveCommand(Command):
  direction: Direction = None

  def _execute(self):
    if not isinstance(self.direction, Direction):
      raise CommandException(f'Invalid direction: {self.direction}')
    moved = self.world.move_object(self.agent, self.direction.value)
    if not moved:
      raise CommandException(f'Movement blocked')
    print(f'[Move {self.direction.name}] {self.agent.name}')
    self.logger.append(f'{self.agent.code}, MOVE, {self.direction.name}')


@dataclass
class UseItemCommand(Command):
  item: Dict = None

  def _execute(self):
    item = self.world.id2obj[self.item['uid']]
    assert isinstance(item, UsableItem)

    agent = self.agent
    agent.remove_from_inventory(item)
    self.logger.append(f'{agent.code}, USE, {item.code}, {len([x for x in agent.inventory if x.code==item.code])}')

    print(f'[Use Item] {agent.name} use {item.name}')
    if item.health_gain:
      new_health = min(self.agent.max_health, self.agent.health + item.health_gain)
      delta_health = new_health - self.agent.health
      sign = 'PLUS' if delta_health >= 0 else 'MINUS'
      agent.health = new_health
      self.logger.append(f'{agent.code}, HP, {sign}, {delta_health}, {agent.health}')

    if item.hunger_gain:
      new_hunger = min(self.agent.max_hunger, self.agent.hunger + item.hunger_gain)
      delta_hunger = new_hunger - self.agent.hunger
      sign = 'PLUS' if delta_hunger >= 0 else 'MINUS'
      agent.hunger = new_hunger
      self.logger.append(f'{agent.code}, HUNGER, {sign}, {delta_hunger}, {agent.hunger}')

    # remove points for point item
    if isinstance(item, PointItem):
      self.world.add_points(agent, -item.points, logger=self.logger)


@dataclass
class TakeItemCommand(Command):
  item: Dict = None

  def _execute(self):
    item = self.world.id2obj[self.item['uid']]
    assert isinstance(item, InventoryItem)

    agent = self.agent
    parent_cell = agent.parent_cell
    if parent_cell is not item.parent_cell:
      raise CommandException('player and item must be in the same spot')

    self.world.remove_object(item)
    agent.add_to_inventory(item)
    row, col = agent.pos

    # hackish
    num_objects = len(parent_cell.objects) - 1
    item_size = len([x for x in agent.inventory if x.code == item.code])
    if num_objects == 0 and parent_cell.has_chest:
      self.logger.append(f'{agent.code}, PICK, {item.code}, {row}_{col}, {item_size}, DESTROY_CHEST')
      parent_cell.has_chest = False
    else:
      self.logger.append(f'{agent.code}, PICK, {item.code}, {row}_{col}, {item_size}')

    print(f'{agent.name} take {item.name}')

    # add points for taking  point item
    if isinstance(item, PointItem):
      self.world.add_points(agent, item.points, logger=self.logger)
