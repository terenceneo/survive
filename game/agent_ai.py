from __future__ import annotations
import random

from game.commands import Command, AttackCommand, TakeItemCommand, MoveCommand,\
  IdleCommand, UseItemCommand


#######################
# Things that can act #
#######################
class AgentAI:
  @staticmethod
  def decide(state, prev_state=None) -> Command:
    return IdleCommand()


class SimpleAI(AgentAI):
  unit_type = ['archer']

  @staticmethod
  def decide(state, prev_state=None) -> Command:
    for attackable in state['attackables']:
      return AttackCommand(attackable)

    for takable in state['takeables']:
      return TakeItemCommand(takable)

    for usable in state['usables']:
      return UseItemCommand(usable)

    random.shuffle(state['movables'])
    for movable in state['movables']:
      return MoveCommand(movable)

    return IdleCommand()


class MonsterSimpleAI(AgentAI):
  @staticmethod
  def decide(state, prev_state=None):
    random.shuffle(state['attackables'])
    for attackable in state['attackables']:
      if 'player_id' in attackable:
        return AttackCommand(attackable)

    #sort indirect_attackables by nearness and then lowest HP for ties
    indirect_attackables = sorted(state['indirect_attackables'],
                                  key=lambda x: (len(x['shortest_path']), x['health']),
                                  reverse=False)

    for agent in indirect_attackables:
      shortest_path = agent['shortest_path']
      if 'player_id' in agent and len(shortest_path) <=3 and random.random() < .80:
        print(f'Chasing {agent["name"]}')
        first_move_dir = shortest_path[0]
        return MoveCommand(first_move_dir)

    random.shuffle(state['movables'])
    for movable in state['movables']:
      return MoveCommand(movable)

    return IdleCommand()


class PlayerOneAI(SimpleAI):
  @staticmethod
  def decide(state, prev_state=None):
    ##################
    # Your Code here #
    ##################

    ##################
    # Your Code here #
    ##################
    return SimpleAI.decide(state)


class PlayerTwoAI(SimpleAI):
  @staticmethod
  def decide(state, prev_state=None):
    return SimpleAI.decide(state)


class PlayerThreeAI(SimpleAI):
  @staticmethod
  def decide(state, prev_state=None):
    return SimpleAI.decide(state)


class PlayerFourAI(SimpleAI):
  @staticmethod
  def decide(state, prev_state=None):
    return SimpleAI.decide(state)


PLAYER_AIS = [PlayerOneAI, PlayerTwoAI, PlayerThreeAI, PlayerFourAI]
