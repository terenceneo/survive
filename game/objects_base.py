from __future__ import annotations
from typing import List, Dict, TYPE_CHECKING, Type
from types import MethodType
from dataclasses import dataclass, InitVar, field, asdict

from func_timeout import func_set_timeout


if TYPE_CHECKING:
  from game.agent_ai import AgentAI, SimpleAI
  from game.cell import Cell


@dataclass(eq=False)
class WorldObject:
  parent_cell: InitVar[Cell] = field(default=None, repr=False)
  name: str = field(default='')
  movement: int = 0
  blocking: bool = field(default=False, repr=False)
  base_atk: int = field(default=0)
  symbol: str = field(default='?')
  code: str = field(default='??', repr=False)
  uid: int = field(default=0, repr=False)

  def __post_init__(self, parent_cell):
    self.parent_cell: Cell = parent_cell
    self.uid = id(self)

  def set_parent_cell(self, cell: Cell):
    self.parent_cell: Cell = cell

  @property
  def pos(self):
    if self.parent_cell:
      return self.parent_cell.pos
    return None

  __hash__ = object.__hash__

  __eq__ = object.__eq__

  @property
  def valid_moves(self):
    return []

  def as_dict(self) -> Dict:
    out = asdict(self)
    if self.parent_cell:
      out['position'] = self.parent_cell.pos
    if 'agent_ai' in out:
      del out['agent_ai']
    return out


@dataclass(eq=False)
class InventoryItem(WorldObject):
  movement: int = 0
  blocking: bool = field(default=False, repr=False)


@dataclass(eq=False)
class Weapon(InventoryItem):
  atk: int = 0


@dataclass(eq=False)
class LivingThing(WorldObject):
  health: int = 50
  max_health: int = 100
  max_hunger: int = 50

  hunger: int = 50
  hunger_delta: int = 1

  blocking: bool = field(default=True, repr=False)
  base_atk = 10


@dataclass(eq=False)
class UsableItem(InventoryItem):
  health_gain: int = 0
  hunger_gain: int = 0


@dataclass(eq=False)
class PointItem(UsableItem):
  point_gain: int = 0


@dataclass(eq=False)
class Agent(LivingThing):
  inventory: List = field(default_factory=list)
  points: int = 10
  gathered_points: int = 0
  agent_ai: Type[AgentAI] = field(default=None)
  timeout: InitVar[int] = None
  agent_range = 1

  def __post_init__(self, parent_cell, timeout, agent_range=1):
    super().__post_init__(parent_cell)
    self.agent_ai = self.agent_ai()
    self.decide = self.agent_ai.decide
    self.agent_range = agent_range
    if timeout:
      self.decide = func_set_timeout(timeout)(self.decide)

  def add_to_inventory(self, item: InventoryItem):
    self.inventory.append(item)

  def remove_from_inventory(self, item: InventoryItem):
    assert item in self.inventory
    self.inventory.remove(item)

  @property
  def is_dead(self):
    return self.health <= 0 or self.parent_cell is None or self.hunger <= 0

  @property
  def character_settings(self):
    out = list()
    out.append(f'Start of {self.code}')

    out.append(f'Health: {self.health}')
    out.append(f'Hunger: {self.hunger}')
    out.append(f'Inventory: {self.inventory}')

    out.append(f'End')
    return '\n'.join(out)

  @property
  def atk(self):
    total_atk = self.base_atk
    for item in self.inventory:
      if isinstance(item, Weapon):
        total_atk += item.atk
    return total_atk
