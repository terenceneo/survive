"""
@author: Vincent Seng
"""
import os
import importlib
import shutil
import subprocess
import time
from functools import wraps, singledispatch, update_wrapper
from types import SimpleNamespace
import types
import json
from pathlib import Path
import math


#########
# Timer #
#########
class Timer:
  """
  import time
  with Timer() as t:
    time.sleep(1)

  print(t.interval)
  """
  def __init__(self, enter_msg='', exit_msg='Elapsed:', verbose=True):
    self.enter_msg = enter_msg
    self.exit_msg = exit_msg
    self.verbose = verbose

  def __enter__(self):
    if self.enter_msg:
      print(self.enter_msg)
    self.start = time.perf_counter()
    return self

  def __exit__(self, *args):
    self.end = time.perf_counter()
    self.interval = self.end - self.start
    if self.verbose:
      print(f"{self.exit_msg} {self.interval:.4f}s")


# decorator
def timeit(fn):
  """
  @timeit
  def foo():
    time.sleep(2)
    return True

  foo()
  """

  @wraps(fn)
  def decorator(*args, **kwargs):
    tic = time.time()
    out = fn(*args, **kwargs)
    toc = time.time()
    print(f"  [timeit] {fn.__name__}(): {toc - tic:.5f}s")
    return out

  return decorator


#################
# Command utils #
#################
def str2bool(s):
  """
  use as argument type
  p.add_argument('--x',type=str2bool, choices=['True', 'False'], default='True')
  """
  if isinstance(s, bool):
    return s
  s = s.lower()
  assert s == 'true' or s == 'false', s
  return s == 'true'


######
# IO #
######
def make_directories(*directories, clear_if_exists=False, src='make_dir', verbose=True):
  for path in directories:
    abs_path = os.path.abspath(path)
    if os.path.exists(abs_path) and not clear_if_exists and verbose:
      print(f'[{src}] Dir exists: {abs_path}')
      continue
    if os.path.exists(abs_path) and clear_if_exists:
      if verbose:
        print(f'[{src}] Clear content: {abs_path}')
      shutil.rmtree(abs_path)
    os.makedirs(abs_path)
    if verbose:
      print(f'[{src}] Make dir: {abs_path}')


def clear_directories(*directories):
  return make_directories(*directories, clear_if_exists=True)


def save_list(lst, path, encoding=None, append=False, print_row=True, verbose=True):
  path = Path(path)
  if verbose:
    print(f'[save_list] {path.name} (#lines {len(lst)}) {path}')
  mode = 'a' if append else 'w'
  with open(path, mode, encoding=encoding) as f:
    for row in lst:
      f.write(row+'\n')
  if print_row and verbose:
    print(f'[save_list] first line: {lst[0:1]}')


def load_list(path, encoding=None, split=True, verbose=True):
  if isinstance(path, str):
    path = Path(path)
  if not os.path.exists(path):
    print('{} does not exist'.format(path))
    return []
  if verbose:
    print(f'[load_list] {path.name} {path}')
  with open(path, "r", encoding=encoding) as f:
    lines = f.read()
    if split:
      lines = lines.splitlines()
  return lines


def save_json(dic, output_path, indent=2, verbose=True):
  with open(output_path, 'w') as f:
    json.dump(dic, f, indent=indent)
  if verbose:
    print(f'[save_json] Saved: {output_path}')


def load_json(path, verbose=True):
  path = Path(path)
  if verbose:
    print(f'[load_json] {path.name} {path}')
  with open(path, 'r') as f:
    dic = json.load(f)
  return dic


##########
# Config #
##########
class Config(SimpleNamespace):
  KEY_OFFSET = 30

  def __init__(self, **kwargs):
    super().__init__(**kwargs)

  @staticmethod
  def load(config_path=None, config_module=None, keep_vars=()):
    assert not(config_path and config_module)

    c_module = config_module
    if config_path:
      print(f"[Config.load] {config_path} [working dir] {os.getcwd()}")
      name = config_path.replace('/', '.')
      c_module = importlib.import_module(name)

    params = dict()
    for k, val in c_module.__dict__.items():
      # ignore modules and private variables
      if isinstance(val, types.ModuleType) or k.startswith('_'):
        continue
      params[k] = val

    for key in keep_vars:
      params[key] = c_module.__dict__[key]

    params['__file__'] = c_module.__file__

    c = Config(**params)
    return c

  def save_file(self, out_path):
    shutil.copy2(self.__file__, out_path)
    print('[save_config] Saved to {}'.format(out_path))

  def save_json(self, out_path):
    with open(out_path, 'w') as f:
      json.dump(self.__dict__, f, indent=2, sort_keys=True, separators=(',', ': '))
    print('[save_config] Saved to {}'.format(out_path))

  def pprint(self, ordered=False):
    print('[Config]')
    params = self.__dict__.items()
    if ordered:
      params = sorted(params)
    for k, v in params:
      print("{key:<{size}} ({_type}): {val}".format(
        key=k, size=self.KEY_OFFSET-len(type(v).__name__), _type=type(v).__name__, val=v))

  def __str__(self):
    params = self.__dict__.items()
    out = ''
    for k, v in params:
      out += "{key:<{size}} ({_type}): {val}\n".format(
        key=k, size=self.KEY_OFFSET-len(type(v).__name__), _type=type(v).__name__, val=v)
    return out


#################
# Command utils #
#################
def run_cmd(cmd, kwargs=None, print_cmd=True, check_returncode=True, stdout=False,
            stderr=False, ignore_exception=True, retry=0):
  if isinstance(cmd, str):
    cmd = cmd.split()

  # extend command
  if kwargs:
    cmd.extend(['{} {}'.format(k, v) for k, v in kwargs.items()])

  if print_cmd:
    print("[run cmd] {}".format(' '.join(cmd)))

  _kwargs = dict()
  if stdout:
    _kwargs['stdout'] = subprocess.PIPE
  if stderr:
    _kwargs['stderr'] = subprocess.PIPE

  completed_proc = subprocess.run(cmd, **_kwargs)

  if check_returncode:
    try:
      completed_proc.check_returncode()
    except subprocess.CalledProcessError as e:
      print('[run cmd][failed] {}'.format(' '.join(cmd)))
      print(completed_proc.stderr.decode("utf-8"))
      if not ignore_exception:
        raise e
      if retry > 0:
        print('retrying')
        completed_proc = run_cmd(cmd, kwargs, print_cmd, check_returncode,
                                 stdout, stderr, ignore_exception, retry-1)

  if print_cmd:
    print('[run cmd] Completed')
  return completed_proc


########
# misc #
########
def print_with_hex_border(ss, indentation=1, newline=1):
  ss=str(ss)
  out = ''
  r, c = indentation, newline
  len_ss = len(ss)
  out += '\n' * c
  out += ' ' * r + '##' + '#' * len_ss + '##\n'
  out += ' ' * r + '# ' + str(ss)      + ' #\n'
  out += ' ' * r + '##' + '#' * len_ss + '##\n'
  out += '\n' * max(c-1, 0)
  print(out)


def reload(lib):
  importlib.reload(lib)


def split_list(lst, num_per_chunk=None, num_chunks=None):
  assert (num_per_chunk and not num_chunks) or (not num_per_chunk and num_chunks)
  if num_per_chunk:
    num_chunks = math.ceil(len(lst)/num_per_chunk)
  if num_chunks:
    num_per_chunk = int(len(lst)/num_chunks)
  return (lst[i * num_per_chunk:(i+1) * num_per_chunk] for i in range(num_chunks))


def method_dispatch(func):
    dispatcher = singledispatch(func)

    def wrapper(*args, **kwargs):
        return dispatcher.dispatch(args[1].__class__)(*args, **kwargs)
    wrapper.register = dispatcher.register
    update_wrapper(wrapper, func)
    return wrapper
