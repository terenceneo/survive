from dataclasses import dataclass, asdict
from .main_utils import save_json


@dataclass
class DataclassUtilsMixin:
  """
  For parameters that are not json serializable,
  """
  def save(self, path):
    save_json(self.as_json(), path, verbose=True)

  def as_json(self):
    return asdict(self)
