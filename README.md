This is a project repository for the WoS Hunger Games software.

# Run simulation
`python client.py`

# Content
Files in PathFinding: Source code for path finding algorithms
Files in root directory besides PathFinding: Main backend logic

# Class Diagram
![Class diagram](resources/class_diagram.png)